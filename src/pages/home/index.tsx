import React, { useState } from "react";
import images from "../../images/image";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// @ts-ignore
import Slider from "react-slick";

function HonePage() {
  const settings = {
    className: "slick_trending",
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
  };

  const [data, setData] = useState<string[]>([
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
  ]);

  return (
    <>
      <div className="header">
        <div className="menu">
          <div className="item_menu">
            <img src={images.ic_logo} alt="logo" />
          </div>
          <div className="item_menu">
            <a href="/" className="item active_menu">
              <img src={images.ic_home} alt="home" /> Trang Chủ
            </a>
          </div>
          <div className="item_menu">
            <a href="/" className="item">
              <img src={images.ic_tivi} alt="home" />
              Truyền hình
            </a>
          </div>
          <div className="item_menu">
            <a href="/" className="item">
              <img src={images.ic_new} alt="home" />
              Tin tức
            </a>
          </div>
          <div className="item_menu">
            <a href="/" className="item">
              <img src={images.ic_game} alt="home" />
              Game Show
            </a>
          </div>
          <div className="item_menu">
            <a href="/" className="item">
              <img src={images.ic_movie} alt="home" />
              Phim Ảnh
            </a>
          </div>
          <div className="item_menu">
            <a href="/" className="item">
              <img src={images.ic_movie} alt="home" />
              Thể thao
            </a>
          </div>
          <div className="item_menu">
            <a href="/" className="item">
              <img src={images.ic_edu} alt="home" />
              Giáo Dục
            </a>
          </div>
        </div>
        {/* Header tag */}
        <div className="header_tag">
          <div className="item_tag">Chương Trình HOT</div>
          <div className="item_tag">Tin Tức Trong Ngày</div>
          <div className="item_tag">24/7</div>
          <div className="item_tag">Hoạt Động Thường Nhật</div>
          <div className="item_tag">Danh Sách Xem Sau</div>
        </div>
      </div>

      <div className="trending">
        <div className="title">Top thịnh hành</div>
        <div className="content_trending">
          <Slider {...settings}>
            {data.map((value: string) => {
              return (
                <div className="list_trending" key={value}>
                  <div>
                    <img
                      src={images.ic_trending}
                      alt="trendings"
                      className="img_trending"
                    />
                  </div>
                  <div className="item_trending">
                    <div className="show_trending">
                      <div
                        style={{
                          width: "40px",
                          height: "40px",
                          borderRadius: "50px",
                          background: "red",
                        }}
                      ></div>
                      <div
                        style={{
                          width: "40px",
                          height: "40px",
                          borderRadius: "50px",
                          background: "red",
                          margin: "0 10px",
                        }}
                      ></div>
                      <div
                        style={{
                          width: "40px",
                          height: "40px",
                          borderRadius: "50px",
                          background: "red",
                        }}
                      ></div>
                    </div>
                    <div>
                      <img src={images.ic_one} alt="one" />
                    </div>
                    <div className="info">
                      <div>Rap việt</div>
                      <div>2020 | Việt Nam | Mùa 1</div>
                    </div>
                  </div>
                </div>
              );
            })}
          </Slider>
        </div>
      </div>
    </>
  );
}

export default HonePage;
