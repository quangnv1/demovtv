import React from "react";
import Slider from "react-slick";
import images from "../../images/image";

function HomeMiddle() {
  const settings = {
    // dots: true,
    // infinite: false,
    // speed: 500,
    centerPadding: "60px",
    slidesToShow: 4,
    slidesToScroll: 1,
    // responsive: [
    //     {
    //       breakpoint: 1024,
    //       settings: {
    //         arrows: false,
    //         centerMode: true,
    //         centerPadding: '40px',
    //         slidesToShow: 6
    //       }
    //     },
    //     {
    //       breakpoint: 768,
    //       settings: {
    //         arrows: false,
    //         centerMode: true,
    //         centerPadding: '40px',
    //         slidesToShow: 4
    //       }
    //     },
    //     {
    //       breakpoint: 480,
    //       settings: {
    //         arrows: false,
    //         centerMode: true,
    //         centerPadding: '40px',
    //         slidesToShow: 1
    //       }
    //     }
    //   ]
  };
  const list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  return (
    <div className="container main">
      <div className="row">
        <div className="col-xl-12">
          <img className="w-100" src={images.ad_banner} alt="" />
        </div>
      </div>
      <div className="row">
        <div className="playing col-12">
          <div className="row my-3">
            <div className="col-12">
              <div className="title-playing w-100 ">
                <span>Đang phát sóng</span>
                <div className="col-1 bottom"></div>
              </div>
            </div>
          </div>
          <div className="row">
            <Slider {...settings}>
              {list.map((item) => {
                return (
                  <div className="block_live_streaming ">
                    <div className="top ">
                      <img
                        className="w-100"
                        src={images.maxresdefault}
                        alt=""
                      />
                      {/* <div className="spot-title">
                                        <img className="text-middle" src={images.round} alt=""/>
                                        <span className="text-uppercase text-middle">live</span>
                                    </div> */}
                    </div>
                    <div className="bottom">Ơn giời cậu đây rồi</div>
                    <div className="tag">
                      <span className="text-uppercase text-middle">live</span>
                    </div>
                    <div className="time">
                      <span className="">Thời gian: </span>
                      <span className="">11:00 22-04-2021</span>
                    </div>
                    <div className="score">
                      <span className="">Tỉ số: </span>
                      <span className="">4 - 0</span>
                    </div>
                  </div>
                );
              })}
            </Slider>
            {/* <div className="col-2 mb-4">
                        
                    </div> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default HomeMiddle;
