const images = {
  ic_logo: require("./logo.svg").default,
  ic_home: require("./home.svg").default,
  ic_tivi: require("./tivi.svg").default,
  ic_new: require("./new.svg").default,
  ic_game: require("./game.svg").default,
  ic_movie: require("./movie.svg").default,
  ic_fb: require("./fb.svg").default,
  ic_edu: require("./edu.svg").default,
  ic_trending: require("./trending.svg").default,
  ad_banner: require("./ad_banner.png").default,
  round: require("./round.png").default,
  maxresdefault: require("./maxresdefault.png").default,
  ic_one: require("./1.svg").default,
};
export default images;
