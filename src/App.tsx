import React from "react";
import "./App.scss";
import HonePage from "./pages/home";
import HomeMiddle from "./pages/home/HomeMiddle";

function App() {
  return (
    <div className="main_content">
      <HonePage />
      <HomeMiddle />
    </div>
  );
}

export default App;
